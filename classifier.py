#!usr/bin/env python

#First test of a Concolutional Neural Network

import tensorflow as tf
import numpy as np
import cv2
import argparse
import sys
import time

from matplotlib import pyplot as plt
from PIL import Image

class MyParser(argparse.ArgumentParser):
	def error(self, message):
		sys.stderr.write('error: %s\n' % message)
		self.print_help()
		sys.exit(2)

parser = argparse.ArgumentParser()

parser.add_argument('-m', dest='MODEL', type=str, help='Path to the saved model')
parser.add_argument('-i', dest='IMAGE', type=str, help='Path to image which should be classified')
if len(sys.argv)==1:
	parser.print_help()
	sys.exit(1)

FLAGS = parser.parse_args()

#python optimisation variables
model_path = FLAGS.MODEL

image_file = FLAGS.IMAGE

def weight_variable(shape):
	initial = tf.truncated_normal(shape, stddev=0.1)	#np.sqrt(2 / np.prod(input.get_shape().as_list()[1:]))
	return tf.Variable(initial)

def bias_variable(shape):
	initial = tf.constant(0.1, shape=shape)
	return tf.Variable(initial)

def conv2d(x, W):
	return tf.nn.conv2d(x, W, strides=[1, 1, 1, 1], padding='SAME')

def max_pool_2x2(x):
	return tf.nn.max_pool(x, ksize=[1, 2, 2, 1], strides=[1, 2, 2, 1], padding='SAME')

def conv_layer(input, shape):
	W = weight_variable(shape)
	b = bias_variable([shape[3]])
	return tf.nn.elu(conv2d(input, W) + b)

def full_layer(input, size):
	in_size = int(input.get_shape()[1])
	W = weight_variable([in_size, size])
	b = bias_variable([size])
	return tf.matmul(input, W) + b

#declaration of training data placeholders
x = tf.placeholder(tf.float32, shape=[None, 32, 32, 3], name="x")		#reshape(tensor, shape[i,j,k,l](i = number of training samples (None = dynamicly reshape based on the number of training samples),
								#				j = height of the image,
								#				k = weight,
								#				l = channel number (because these are RGB images l is 3))
y = tf.placeholder(tf.float32, [None, 43])			#43 digits
keep_prob = tf.placeholder(tf.float32)

#creating of convolutional layers
with tf.name_scope("conv_"):
	layer1 = conv_layer(x, shape=[3, 3, 3, 32])
	layer1_pool = max_pool_2x2(layer1)
	layer2 = conv_layer(layer1_pool, shape=[3, 3, 32, 64])
	layer2_pool = max_pool_2x2(layer2)
	layer3 = conv_layer(layer2_pool, shape=[3, 3, 64, 128])
	layer3_pool = max_pool_2x2(layer3)
	layer3_flat = tf.reshape(layer3_pool, [-1, 4 * 4 * 128])
	layer3_drop = tf.layers.dropout(layer3_flat, rate=keep_prob)

with tf.name_scope("full_"):	
	connected1 = tf.nn.elu(full_layer(layer3_flat, 1024))
	connected1_drop = tf.layers.dropout(connected1, rate=keep_prob)

with tf.name_scope("out_"):
	layer = full_layer(connected1, 43)

prediction = tf.argmax(layer, 1, name="predict")

#setup of the initialisation operator
init_op = tf.global_variables_initializer()

saver = tf.train.Saver()	#saver

#setting up of the training
with tf.Session() as sess:
	#initialisation of the variables
	sess.run(init_op)

	saver.restore(sess, model_path)

	image = cv2.imread(image_file, 1)
	image = cv2.resize(image, (32,32))
	image = np.reshape(image, (1,32,32,3))

	predict = sess.run(prediction, feed_dict={x:image})
	
	txtFile = open("/tmp/class.txt", "w")
	txtFile.write(str(predict[0]))
	txtFile.close()
