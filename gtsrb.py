#!usr/bin/env python

#First test of a Concolutional Neural Network

import tensorflow as tf
import numpy as np
import time
from tqdm import tqdm

import gtsrb_input

#python optimisation variables
learning_rate = 0.999
epochs = 200
batch_size = 380
model_path = "model/model.ckpt"

def weight_variable(shape):
	initial = tf.truncated_normal(shape, stddev=0.1)	#np.sqrt(2 / np.prod(input.get_shape().as_list()[1:]))
	return tf.Variable(initial)

def bias_variable(shape):
	initial = tf.constant(0.1, shape=shape)
	return tf.Variable(initial)

def conv2d(x, W):
	return tf.nn.conv2d(x, W, strides=[1, 1, 1, 1], padding='SAME')

def max_pool_2x2(x):
	return tf.nn.max_pool(x, ksize=[1, 2, 2, 1], strides=[1, 2, 2, 1], padding='SAME')

def conv_layer(input, shape):
	W = weight_variable(shape)
	b = bias_variable([shape[3]])
	return tf.nn.elu(conv2d(input, W) + b)

def full_layer(input, size):
	in_size = int(input.get_shape()[1])
	W = weight_variable([in_size, size])
	b = bias_variable([size])
	return tf.matmul(input, W) + b

#declaration of training data placeholders
x = tf.placeholder(tf.float32, shape=[None, 32, 32, 3], name="x")		#reshape(tensor, shape[i,j,k,l](i = number of training samples (None = dynamicly reshape based on the number of training samples),
								#				j = height of the image,
								#				k = weight,
								#				l = channel number (because these are RGB images l is 3))
y = tf.placeholder(tf.float32, [None, 43])			#43 digits
keep_prob = tf.placeholder(tf.float32)

#creating of convolutional layers
with tf.name_scope("conv_"):
	layer1 = conv_layer(x, shape=[3, 3, 3, 32])
	layer1_pool = max_pool_2x2(layer1)
	layer2 = conv_layer(layer1_pool, shape=[3, 3, 32, 64])
	layer2_pool = max_pool_2x2(layer2)
	layer3 = conv_layer(layer2_pool, shape=[3, 3, 64, 128])
	layer3_pool = max_pool_2x2(layer3)
	layer3_flat = tf.reshape(layer3_pool, [-1, 4 * 4 * 128])
	layer3_drop = tf.layers.dropout(layer3_flat, rate=keep_prob)

with tf.name_scope("full_"):	
	connected1 = tf.nn.elu(full_layer(layer3_drop, 1024))
	connected1_drop = tf.layers.dropout(connected1, rate=keep_prob)

with tf.name_scope("out_"):
	layer = full_layer(connected1_drop, 43)

#computes softmax cross entropy between logits and labels
cross_entropy = tf.reduce_mean(tf.nn.softmax_cross_entropy_with_logits(logits=layer, labels=y))		#measures the probability error in descrete classification tasks

#adding of the optimiser
optimiser = tf.train.AdadeltaOptimizer(learning_rate=learning_rate).minimize(cross_entropy)			#using the adadelta algorithm to minimize the error-rate of the cross entropy

#definition of an accuracy assessment operation
correct_prediction = tf.equal(tf.argmax(layer, 1), tf.argmax(y, 1))		#equals the created placeholder with the expected output
accuracy = tf.reduce_mean(tf.cast(correct_prediction, tf.float32))		#creates the accuracy rate from former correct prediction

prediction = tf.argmax(layer, 1, name="prediction")

#setup of the initialisation operator
init_op = tf.global_variables_initializer()

#added a summary to store the accuracy
tf.summary.scalar('accuracy', accuracy)

saver = tf.train.Saver()	#saver

merged = tf.summary.merge_all()				#merges all summaries collected in the default graph
writer = tf.summary.FileWriter('logdata/gtsrb/test_false_classes_4')	#creation of the event file which contains the summaries and events

#setting up of the training
with tf.Session() as sess:
	#initialisation of the variables
	sess.run(init_op)
	
	start_time = int(time.time())
	for epoch in range(epochs):
		total_loss = 0
		class_num = 0
		inclass_num = 0
		total_batches = 39209 / batch_size
		pbar = tqdm(total=total_batches)	#progressbar
		for k in range(total_batches):
			images, labels, class_num, inclass_num = gtsrb_input.readTrainTrafficSigns(class_num, inclass_num, batch_size)
			_, loss = sess.run([optimiser, cross_entropy], feed_dict={x: images, y: labels, keep_prob: 0.9})
			total_loss += loss
			pbar.update(1)
		pbar.close()
		test_x, test_y = gtsrb_input.readTestTrafficSigns()
		X = np.reshape(test_x, (10, 1000, 32, 32, 3))
		Y = np.reshape(test_y, (10, 1000, 43))
		test_x = np.reshape(test_x, (10000, 32, 32, 3))
		test_y = np.reshape(test_y, (10000, 43))		
		acc = np.mean([sess.run(accuracy, feed_dict={x: X[i], y: Y[i], keep_prob: 1.0}) for i in range(10)])
		summary = sess.run(merged, feed_dict={x: test_x, y: test_y, keep_prob:1.0})
		writer.add_summary(summary, epoch)
		print("Epoch: {:>3} = Loss: {:05.2f} ; Accuracy: {:04.2f}%".format(epoch + 1, total_loss, acc * 100))
		if epoch == epochs-1:
			while k in range(10000):
				pred_x = np.reshape(test_x[k], (1,32,32,3))
				prediction = sess.run(prediction, feed_dict={x:pred_x})
				if prediction[0] != tf.argmax(test_y[k],1):
					print("\nFalse predicted class: {}".format(prediction))
	
	total_minutes = (int(time.time()) - start_time) / 60
	hours = int(total_minutes / 60)
	minutes = total_minutes - (hours * 60)
	print("\nTraining completed in {}:{:0>2} hours!".format(hours, minutes))
	saver.save(sess, model_path)		#save model
	writer.add_graph(sess.graph)
