#!usr/bin/env python

import csv
import cv2
import pickle
import numpy as np
import tensorflow as tf

from itertools import islice, chain

TRAIN_SET_DIR = 'traffic-signs/GTSRB/Final_Training/Images'
TEST_SET_DIR = 'traffic-signs/GTSRB/Final_Test/Images'

trainImages = []
trainLabels = []
testImages = []
testLabels = []

def adjust_gamma(image, gamma):
	invGamma = 1.0/gamma
	table = np.array([((i/255.0)**invGamma)*255
				   for i in np.arange(0,256)]).astype("uint8")
	return cv2.LUT(image,table)
	
def data_augmentation(img):
	blur = cv2.blur(img, (5,5))
	gaus = cv2.GaussianBlur(img, (5,5), 0)
	median = cv2.medianBlur(img, 5)
	
	for gamma in np.arange(0.5, 2.5, 0.5):
		if gamma == 1:
			continue
		
		adjusted = adjust_gamma(img, gamma)
		if gamma == 0.5:
			img_half = adjusted
		if gamma == 1.5:
			img_fifty = adjusted
		if gamma == 2.0:
			img_double = adjusted
			
	return blur, gaus, median, img_half, img_fifty, img_double

def readTrainTrafficSigns(class_counter, inclass_counter, batch_size):
	"""
	Reading of the train dataset.
	
	Params
	----------
	class_num:	int
			identifier for the input class

	Returns
	----------
	"""
	images = []
	labels = []
	next_counter = 0
	c_counter = class_counter
	ic_counter = inclass_counter
	counter = 0
	first = True
	batch_full = False

	for i in range(0,43):
		if c_counter != 0:									# is this the first load in this epoch?
			i = c_counter
		prefix = TRAIN_SET_DIR + '/' + format(i, '05d') + '/'
		gtFile = open(prefix + 'GT-' + format(i, '05d') + '.csv')
		gtReader = csv.reader(gtFile, delimiter=';')
		gtReader.next()										# jump over the header
		if first == False:									# is this the first load in this training session?
			ic_counter = 0
		first = False

		for row in gtReader:
			if next_counter != ic_counter:							# check if I need to change the row
				while next_counter != ic_counter:					#
					gtReader.next()							#
					next_counter += 1						#
			image = cv2.imread(prefix + row[0], 1)
			image = cv2.resize(image, (32, 32))
			image1, image2, image3, image4, image5, image6 = data_augmentation(image)
			image = image.astype(float) / 255
			image1 = image1.astype(float) / 255
			image2 = image2.astype(float) / 255
			image3 = image3.astype(float) / 255
			image4 = image4.astype(float) / 255
			image5 = image5.astype(float) / 255
			image6 = image6.astype(float) / 255
			images.extend([image, image1, image2, image3, image4, image5, image6])
			label = np.eye(1, 43, int(row[7]), dtype=float)
			label = np.squeeze(label)
			lc = 0
			while lc < 7:
				labels.append(label)
				lc += 1
			next_counter += 1
			ic_counter += 1
			counter += 1
			if counter == batch_size:
				batch_full = True
				break

		#print("out of read loop")
		#c_counter += 1
		gtFile.close()
		if batch_full == True:
			break
		else:
			next_counter = 0
			ic_counter = 0
			c_counter += 1

	#print("output")
	trainImages = images
	trainLabels = labels
	
	return trainImages, trainLabels, c_counter, ic_counter

def readTestTrafficSigns():#count, batch_size):
	"""
	Reading of the test dataset.
	
	Params
	----------

	Returns
	----------
	"""
	images = []
	labels = []
	counter = 0
	#row_counter = 0
	#ic_counter = count
	#batch = batch_size
	
	prefix = TEST_SET_DIR + '/'
	gtFile = open(prefix + 'GT-final_test.csv')
	gtReader = csv.reader(gtFile, delimiter=';')
	gtReader.next()
	
	for row in gtReader:
		#if row_counter != ic_counter:
		#	while row_counter != ic_counter:
		#		gtReader.next()
		#		row_counter += 1
		image = cv2.imread(prefix + row[0], 1)
		image = cv2.resize(image, (32, 32))
		image = image.astype(float) / 255
		images.append(image)
		labels.append(np.squeeze(np.eye(1, 43, int(row[7]), dtype=float)))
		#ic_counter += 1
		#row_counter += 1
		counter += 1
		if counter == 10000:
			break

	gtFile.close()

	testImages = images
	testLabels = labels

	return testImages, testLabels#, ic_counter

#image, label = readTestTrafficSigns()
#print(image[0].shape)
#X = np.reshape(image, (10, 1000, 32, 32, 3))
#print("------------------------------------------")
#print(X[0][0].shape)
#print(label[0].dtype)
